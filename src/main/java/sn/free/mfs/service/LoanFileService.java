package sn.free.mfs.service;

import sn.free.mfs.domain.enumeration.GrantingStatus;
import sn.free.mfs.service.dto.LoanFileDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.LoanFile}.
 */
public interface LoanFileService {

    /**
     * Save a loanFile.
     *
     * @param loanFileDTO the entity to save.
     * @return the persisted entity.
     */
    LoanFileDTO save(LoanFileDTO loanFileDTO);

    LoanFileDTO saveMerge(LoanFileDTO loanFileDTO);

    /**
     * Get all the loanFiles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<LoanFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" loanFile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<LoanFileDTO> findOne(Long id);

    /**
     * Delete the "id" loanFile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Check eligibility process and update file
     *
     * @param loanFileDTO file
     * @return the updated file
     */
    LoanFileDTO checkEligibility(LoanFileDTO loanFileDTO);

    LoanFileDTO checkEligibilityUssd(LoanFileDTO loanFileDTO);

    /**
     * Refresh status of a loanfile and create repaymentfile if OK
     *
     * @param loanId id
     * @return the loanFile
     */
    LoanFileDTO refresh(@Valid Long loanId);

    List<LoanFileDTO> findByMsisdnOrOfferSlugOrGrantingStatus(Pageable pageable, String msisdn, String offer, GrantingStatus status, Instant debut, Instant fin);
}
