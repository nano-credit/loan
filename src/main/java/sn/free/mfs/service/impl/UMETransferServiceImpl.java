package sn.free.mfs.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.mfs.domain.UMETransfer;
import sn.free.mfs.repository.UMETransferRepository;
import sn.free.mfs.service.LoanUtils;
import sn.free.mfs.service.UMETransferService;
import sn.free.mfs.service.dto.UMETransferDTO;
import sn.free.mfs.service.mapper.UMETransferMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UMETransfer}.
 */
@Service
@Transactional
public class UMETransferServiceImpl implements UMETransferService {

    private final Logger log = LoggerFactory.getLogger(UMETransferServiceImpl.class);

    private final UMETransferRepository uMETransferRepository;

    private final UMETransferMapper uMETransferMapper;

    public UMETransferServiceImpl(UMETransferRepository uMETransferRepository, UMETransferMapper uMETransferMapper) {
        this.uMETransferRepository = uMETransferRepository;
        this.uMETransferMapper = uMETransferMapper;
    }

    /**
     * Save a uMETransfer.
     *
     * @param uMETransferDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UMETransferDTO save(UMETransferDTO uMETransferDTO) {
        if (uMETransferDTO.getId() != null) { // updating
            UMETransferDTO transfer = findOne(uMETransferDTO.getId()).orElseThrow(IllegalAccessError::new);
            try {
                LoanUtils.merge(uMETransferDTO, transfer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        log.debug("Request to save UMETransfer : {}", uMETransferDTO);
        UMETransfer uMETransfer = uMETransferMapper.toEntity(uMETransferDTO);
        uMETransfer = uMETransferRepository.save(uMETransfer);
        return uMETransferMapper.toDto(uMETransfer);
    }

    /**
     * Get all the uMETransfers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UMETransferDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UMETransfers");
        return uMETransferRepository.findAllByOrderByCreatedDateDesc(pageable)
            .map(uMETransferMapper::toDto);
    }

    /**
     * Get one uMETransfer by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UMETransferDTO> findOne(Long id) {
        log.debug("Request to get UMETransfer : {}", id);
        return uMETransferRepository.findById(id)
            .map(uMETransferMapper::toDto);
    }

    /**
     * Delete the uMETransfer by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UMETransfer : {}", id);
        uMETransferRepository.deleteById(id);
    }

    @Override
    public Optional<List<UMETransferDTO>> findLoanTransfers(Long id) {
        List<UMETransfer> allByLoanFileId = this.uMETransferRepository.findAllByLoanFileIdOrderByCreatedDateDesc(id);
        if (allByLoanFileId == null || allByLoanFileId.isEmpty()) return Optional.empty();
        return Optional.of(allByLoanFileId.stream().map(uMETransferMapper::toDto).collect(Collectors.toList()));
    }
}
