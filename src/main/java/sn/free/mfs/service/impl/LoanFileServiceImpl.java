package sn.free.mfs.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sn.free.mfs.client.AccessListDTO;
import sn.free.mfs.client.GatewayConfigFeignClient;
import sn.free.mfs.client.OfferDTO;
import sn.free.mfs.domain.*;
import sn.free.mfs.domain.enumeration.*;
import sn.free.mfs.repository.CustomerDataRepository;
import sn.free.mfs.repository.LoanFileRepository;
import sn.free.mfs.service.LoanFileSericeAsync;
import sn.free.mfs.service.LoanFileService;
import sn.free.mfs.service.LoanUtils;
import sn.free.mfs.service.UMETransferService;
import sn.free.mfs.service.dto.LoanFileDTO;
import sn.free.mfs.service.dto.UMETransferDTO;
import sn.free.mfs.service.mapper.LoanFileMapper;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.validation.Valid;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service Implementation for managing {@link LoanFile}.
 */
@Service
@Transactional
public class LoanFileServiceImpl implements LoanFileService {

    private final Logger log = LoggerFactory.getLogger(LoanFileServiceImpl.class);

    private final LoanFileRepository loanFileRepository;
    private final LoanFileMapper loanFileMapper;
    private final CustomerDataRepository customerRepo;
    private final GatewayConfigFeignClient gatewayClient;
    private final UMETransferService uMETransferService;
    private final LoanFileSericeAsync loanFileServiceAsync;

    @Value("${app.wallets.freemoney-fees.msisdn:}")
    private String fmCentralFees;
    @Value("${app.jwt}")
    private String jwt;
    @Value("${app.talend.host}")
    private String talendHost;
    @Value("${app.talend.getUserDetailsEndpoint}")
    private String getUserDetailsEndpoint;
    @Value("${app.talend.checkCurrentLoanEndpoint}")
    private String checkCurrentLoanEndpoint;
    @Value("${app.talend.checkAllLoansEndpoint}")
    private String checkAllLoansEndpoint;
    @Value("${app.talend.getBalanceEndpoint}")
    private String getBalanceEndpoint;
    @Value("${app.maxCumulatedBalancePermitted}")
    private double maxCumulatedBalancePermitted;

    public LoanFileServiceImpl(LoanFileRepository loanFileRepository, LoanFileMapper loanFileMapper, CustomerDataRepository customerRepo, GatewayConfigFeignClient gatewayClient, UMETransferService uMETransferService, LoanFileSericeAsync loanFileServiceAsync) {
        this.loanFileRepository = loanFileRepository;
        this.loanFileMapper = loanFileMapper;
        this.customerRepo = customerRepo;
        this.gatewayClient = gatewayClient;
        this.uMETransferService = uMETransferService;
        this.loanFileServiceAsync = loanFileServiceAsync;
    }

    /**
     * Save a loanFile.
     *
     * @param loanFileDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public LoanFileDTO save(LoanFileDTO loanFileDTO) {
        log.debug("Request to save LoanFile : {}", loanFileDTO);
        if (loanFileDTO.getId() == null) {
            loanFileDTO.setGrantingStatus(GrantingStatus.SUBMITTED);
            loanFileDTO.setEligibilityScore(0);
        }

        LoanFile loanFile = loanFileMapper.toEntity(loanFileDTO);
        loanFile = loanFileRepository.save(loanFile);
        return loanFileMapper.toDto(loanFile);
    }

    @Override
    public LoanFileDTO saveMerge(LoanFileDTO loanFileDTO) {
        if (loanFileDTO.getId() != null) { // updating
            LoanFileDTO loanFileDTO1 = findOne(loanFileDTO.getId()).orElseThrow(IllegalAccessError::new);
            try {
                LoanUtils.merge(loanFileDTO, loanFileDTO1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        log.debug("Request to save UMETransfer : {}", loanFileDTO);
        LoanFile loanFile = loanFileMapper.toEntity(loanFileDTO);
        loanFile = loanFileRepository.save(loanFile);
        return loanFileMapper.toDto(loanFile);
    }

    /**
     * Get all the loanFiles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<LoanFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all LoanFiles");
        return loanFileRepository.findAllByOrderByCreatedDateDesc(pageable)
            .map(loanFileMapper::toDto);
    }

    /**
     * Get one loanFile by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<LoanFileDTO> findOne(Long id) {
        log.debug("Request to get LoanFile : {}", id);
        return loanFileRepository.findById(id)
            .map(loanFileMapper::toDto);
    }

    /**
     * Delete the loanFile by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete LoanFile : {}", id);
        loanFileRepository.deleteById(id);
    }

    /**
     * Check Eligibility for both context
     *
     * @param loanFileDTO the loanFile.
     */
    private LoanFileDTO checkEligibilityProcess(LoanFileDTO loanFileDTO) {

        OfferDTO offerConfig = getOfferConfig(loanFileDTO.getOfferSlug());
        // DO ALL CHECK HERE -----vvv
        /*
         * check offer != null if so failed
         * check all in config
         *      and if failed set status to NOT-GRANTED and add specific message
         * Dont forget RG in BL3 -- todo ousmane
         */
        if (offerConfig == null) {
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Offer is null");
            return loanFileDTO;
        }


        //check pret en cours
        RestTemplate rt = new RestTemplate(new BufferingClientHttpRequestFactory(new
            SimpleClientHttpRequestFactory()));
        String urii = UriComponentsBuilder.fromHttpUrl(this.talendHost+this.checkCurrentLoanEndpoint)
            .queryParam("msisdn", loanFileDTO.getMsisdn())
            .queryParam("offre", loanFileDTO.getOfferSlug())
            .toUriString();
        final ResponseEntity<USSDResponse> response = rt.getForEntity(urii, USSDResponse.class);
        if (!response.getStatusCode().is2xxSuccessful() || response.getBody() == null) {
            loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
            loanFileDTO.setGrantingMessage("Cher client, suite a un incident technique, votre demande ne peut etre traitee. Veuillez reessayer ulterieurement");
            return loanFileDTO;
        }
        final USSDResponse bodyLoan = response.getBody();
        final Param resultCode = Arrays.stream(bodyLoan.getParams())
            .filter(p -> p.getName().equals("resultCode"))
            .findFirst()
            .orElse(null);
        if (resultCode != null && resultCode.getValue().equals("1")) {
            final Param enCours = Arrays.stream(bodyLoan.getParams())
                .filter(p -> p.getName().equals("enCours"))
                .findFirst()
                .orElse(null);
            final Param echeance = Arrays.stream(bodyLoan.getParams())
                .filter(p -> p.getName().equals("echeance"))
                .findFirst()
                .orElse(null);
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, vous avez deja un LEBALMA de " + enCours.getValue() + " FCFA en cours sur ce numero, merci de le rembourser avant le " + echeance.getValue());
            return loanFileDTO;
        }



        //check Multi sim abonne if a loan with another or same slug on any other SIM possessed
        /*
        - if total loans > max simultaneous loan
        - pour le moment check juste si un de ces autres numeros a un pret et ==> vous avez deja un pret sur un autre numero
         */
        rt = new RestTemplate(new BufferingClientHttpRequestFactory(new
            SimpleClientHttpRequestFactory()));
        urii = UriComponentsBuilder.fromHttpUrl(this.talendHost+this.checkAllLoansEndpoint)
            .queryParam("idNumber", loanFileDTO.getIdNumber())
            .queryParam("idType", loanFileDTO.getIdType())
            .queryParam("offre", loanFileDTO.getOfferSlug())
            .toUriString();
        final ResponseEntity<USSDResponse> ress = rt.getForEntity(urii, USSDResponse.class);
        if (!ress.getStatusCode().is2xxSuccessful() || ress.getBody() == null) {
            loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
            loanFileDTO.setGrantingMessage("Cher client, suite a un incident technique, votre demande ne peut etre traitee. Veuillez reessayer ulterieurement");
            return loanFileDTO;
        }
        final USSDResponse bodyLoaan = ress.getBody();
        final Param resultCodee = Arrays.stream(bodyLoaan.getParams())
            .filter(p -> p.getName().equals("resultCode"))
            .findFirst()
            .orElse(null);
        if (resultCodee != null && resultCodee.getValue().equals("1")) {
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher Client, vous avez deja un pret en cours sur un autre numero. Veuillez le rembourser pour pouvoir emprunter sur votre numero");
            return loanFileDTO;
        }

        //Access list check
        if (StringUtils.isNotBlank(loanFileDTO.getIdNumber())) {
            AccessListDTO accessListDTO = getBlackOrWhiteList(loanFileDTO.getIdNumber());
            if (accessListDTO != null) {
                if (accessListDTO.getStatus() == AccessStatus.BLACKLISTED) {
                    loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                    loanFileDTO.setGrantingMessage("Cher client, suite a un incident de remboursement, votre compte n’est pas autorise a acceder a ce service");
                    return loanFileDTO;
                }
                if (loanFileDTO.getMsisdn().equals(accessListDTO.getMsisdn()) && accessListDTO.getStatus() == AccessStatus.WHITELISTED ) {
                    if (offerConfig.getMinAmount() != null && offerConfig.getMaxAmount() != null) {
                        if (loanFileDTO.getAmount() < offerConfig.getMinAmount()
                            || loanFileDTO.getAmount() > offerConfig.getMaxAmount()) {
                            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                            loanFileDTO.setGrantingMessage("Cher client, le montant demandé n'est pas autorisé. Merci de revoir le montant saisi");
                            return loanFileDTO;
                        }
                    }
                    loanFileDTO.setGrantingStatus(GrantingStatus.ACCEPTED);
                    loanFileDTO.setGrantingMessage("Votre LEBALMA a reussi. Les fonds seront credites sur votre compte Free Money sous peu");
                    return loanFileDTO;
                }
            }
        }


        //check idType
        if (loanFileDTO.getIdType() == null || !loanFileDTO.getIdType().equals(IDType.CNI)) {
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, vous devez disposer d'une CNI Senegalaise valide pour etre eligible au service LEBALMA. Contactez le Service Client au 201 123 pour plus d infos");
            return loanFileDTO;
        }

        //GetUserDetails
        RestTemplate rs = new RestTemplate(new BufferingClientHttpRequestFactory(new
            SimpleClientHttpRequestFactory()));
        String uri = UriComponentsBuilder.fromHttpUrl(this.talendHost+this.getUserDetailsEndpoint)
            .queryParam("MSISDN", loanFileDTO.getMsisdn()).toUriString();
        final ResponseEntity<USSDResponse> resp = rs.getForEntity(uri, USSDResponse.class);
        if (!resp.getStatusCode().is2xxSuccessful() || resp.getBody() == null) {
            loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
            loanFileDTO.setGrantingMessage("Cher client, suite a un incident technique, votre demande ne peut etre traitee. Veuillez reessayer ulterieurement");
            return loanFileDTO;
        }
        final USSDResponse body = resp.getBody();
        final Param txnStatus = Arrays.stream(body.getParams())
            .filter(p -> p.getName().equals("txnStatus"))
            .findFirst()
            .orElse(null);
        if (txnStatus != null && !txnStatus.getValue().equals("200")) {
            //should never happen because of pin check done on talend
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, pour devenir eligible, vous devez avoir un compte FREE MONEY");
            return loanFileDTO;
        }

        //check identity card given against the one returned by getUserDetails idNumber
        final Param idNumber = Arrays.stream(body.getParams())
            .filter(p -> p.getName().equals("idNumber"))
            .findFirst()
            .orElse(null);
        if(loanFileDTO.getIdNumber()==null){
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, merci de fournir un numero de CNI");
            return loanFileDTO;
        }
        if(idNumber!=null && !idNumber.getValue().equals(loanFileDTO.getIdNumber())){
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, le numéro de CNI saisi est différent de celui communiqué lors de l'ouverture de votre compte Free Money");
            return loanFileDTO;
        }


        //check FREEMONEY group
        final Param detailUserType = Arrays.stream(body.getParams())
            .filter(p -> p.getName().equals("detailUserType"))
            .findFirst()
            .orElse(null);
        if(detailUserType != null && !liquibase.util.StringUtils.isEmpty(detailUserType.getValue())){
            if(!detailUserType.getValue().equals("SUBSCRIBER")){
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Cher client, les comptes Agents et Entreprises n'ont pas encore acces a ce service. Merci de votre comprehension");
                return loanFileDTO;
            }
        }


        //check FREEMONEY account status
        final Param detailGradeCode = Arrays.stream(body.getParams())
            .filter(p -> p.getName().equals("detailGradeCode"))
            .findFirst()
            .orElse(null);
        if(detailGradeCode != null && !liquibase.util.StringUtils.isEmpty(detailGradeCode.getValue())){
            if(!detailGradeCode.getValue().equals("KYC2")){
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Cher Client, votre compte Free Money n’est pas bien identifie. Merci de deplafonner votre compte sur Myfree ou en agence pour etre eligble au service LEBALMA");
                return loanFileDTO;
            }else{
                //KYC2 avec passeport
                if(loanFileDTO.getIdType() != IDType.CNI){
                    loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                    loanFileDTO.setGrantingMessage("Cher client, vous devez disposer d’une CNI Senegalaise valide pour etre eligible a ce service. Contactez le Service Client au 201 123 pour plus d infos");
                    return loanFileDTO;
                }
            }
        }

        //check if customer is on scoring table
        Optional<CustomerData> customerDataOptional = this.customerRepo.findByMsisdn(loanFileDTO.getMsisdn());
        if (!customerDataOptional.isPresent()) {
            loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
            loanFileDTO.setGrantingMessage("Vous n etes pas encore eligible au LEBALMA. Utilisez plus souvent les differents services de votre Kalpe pour etre eligible");
            return loanFileDTO;
        }

        CustomerData customerData = customerDataOptional.get();

        int maxLoanSegment =  Optional.ofNullable(customerData.getMaxLoanSegment()).orElse(0);

        if ( maxLoanSegment == 0) {
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Desole, vous n'etes pas eligible au service LEBALMA. Nous vous invitons a utiliser votre Kalpe Free Money pour toutes vos transactions afin d etre eligible");
            return loanFileDTO;
        }


        //TODO check nombre d avances maximum par jour sur la plateforme max_loan_per_date
        /*
        - check nombre d avances maximum (illimite pour le moment manam 0 ou null
         */

        //Age check +60
        if (offerConfig.getMinCustAge() != null && offerConfig.getMaxCustAge() != null) {
            if (customerData.getAgeClient() > offerConfig.getMaxCustAge()) {
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Cher client, votre compte Free Money ne vous permet pas d’acceder au service LEBALMA. Contactez le Service Client 201123 pour plus d'infos");
                return loanFileDTO;

            }
            else if (customerData.getAgeClient() < offerConfig.getMinCustAge()){
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Vous n'etes pas éligible au service LEBALMA. L'age minimum pour beneficier du service est de " + offerConfig.getMinCustAge() + " ans");
                return loanFileDTO;
            }
        }


        //anciennete MFS
        if (offerConfig.getFreemoneySeniority() != null) {
            if (customerData.getAgeMobileMoneyMonthI() < offerConfig.getFreemoneySeniority()) {
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Desole, vous n'etes pas eligible au service LEBALMA. Votre anciennete sur Free Money est inferieure a " + offerConfig.getFreeSeniority() + " mois. Merci de réessayer ultérieurement");
                return loanFileDTO;
            }
        }

        //anciennete Free
        if (offerConfig.getFreeSeniority() != null) {
            if (customerData.getAgeTelecomMonthI() < offerConfig.getFreeSeniority()) {
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Desole, vous n'etes pas eligible au service LEBALMA. Votre anciennete sur le réseau Free est inferieure a " + offerConfig.getFreeSeniority() + " mois. Merci de réessayer ultérieurement");
                return loanFileDTO;
            }
        }

        //TODO limite d octroi
        /*
         - check amount against cumulated amount on current period (6h-21h or 21h-6h) and accepter le dernier octroi puis refuser tout nouvel jusqu a 6h
         */
        //


        //check amount against solde wallet credit, amount should not be greater
        RestTemplate restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(new
            SimpleClientHttpRequestFactory()));
        String uriString = UriComponentsBuilder.fromHttpUrl(this.talendHost+this.getBalanceEndpoint)
            .queryParam("msisdn", offerConfig.getLoanWallet().getMsisdn())
            .toUriString();
        final ResponseEntity<USSDResponse> ussdResponseResponseEntity = restTemplate.getForEntity(uriString, USSDResponse.class);
        if (!ussdResponseResponseEntity.getStatusCode().is2xxSuccessful() || ussdResponseResponseEntity.getBody() == null) {
            loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
            loanFileDTO.setGrantingMessage("Cher client, suite a un incident technique, votre demande ne peut etre traitee. Veuillez reessayer ulterieurement");
            return loanFileDTO;
        }
        final USSDResponse bodyGetBalance = ussdResponseResponseEntity.getBody();
        final Param resultCode1 = Arrays.stream(bodyGetBalance.getParams())
            .filter(p -> p.getName().equals("resultCode"))
            .findFirst()
            .orElse(null);
        final Param transactionStatus = Arrays.stream(bodyGetBalance.getParams())
            .filter(p -> p.getName().equals("transactionStatus"))
            .findFirst()
            .orElse(null);
        if (resultCode1 != null && !resultCode1.getValue().equals("200")) {
            log.debug("body getbalance : {}, {}", bodyGetBalance, bodyGetBalance.toString());
            log.debug("resultCode getbalance : {}, {}, {}", resultCode1, resultCode1.getName(), resultCode1.getValue());
            log.debug("transactionStatus getbalance : {}, {}, {}", transactionStatus, transactionStatus.getName(), transactionStatus.getValue());
            loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
            loanFileDTO.setGrantingMessage("Cher client, suite a un incident technique, votre demande ne peut etre traitee. Veuillez reessayer ulterieurement");
            return loanFileDTO;
        }
        final Param balance = Arrays.stream(bodyGetBalance.getParams())
            .filter(p -> p.getName().equals("balance"))
            .findFirst()
            .orElse(null);
        if(balance!=null && loanFileDTO.getAmount() > Double.valueOf(balance.getValue())){
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, le montant demande n est pas disponible actuellement. Veuillez reessayer ulterieurement");
            return loanFileDTO;
        }

        //current balance check
        log.debug("balance given {}", loanFileDTO.getBalance());
        if (loanFileDTO.getBalance() == null){
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Balance du client non fournie");
            return loanFileDTO;
        }
        if (offerConfig.getMaxCurrentBalance() != null) {
            if (loanFileDTO.getBalance().intValue() > offerConfig.getMaxCurrentBalance()) {
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Cher client, le solde de votre compte Free Money dépasse le maximum autorisé pour beneficicier du service LEBALMA. Veuillez utiliser votre kalpé Free Money et réessayer ultérieurement");
                return loanFileDTO;
            }
        }

        //check actual balance + received amount < = 2 000 000
        final double fees = loanFileDTO.getAmount() * (offerConfig.getTotalFeesRate() / 100.0);
        final double grantedAmt = loanFileDTO.getAmount() - fees;
        if(grantedAmt + loanFileDTO.getBalance() > maxCumulatedBalancePermitted){
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Cher client, le solde de votre compte Free Money est plafonne a 2 000 000F. Veuillez revoir le montant de votre LEBALMA");
            return loanFileDTO;
        }


        if (offerConfig.getMinAmount() != null && offerConfig.getMaxAmount() != null) {
            if (loanFileDTO.getAmount() < offerConfig.getMinAmount()
                || loanFileDTO.getAmount() > offerConfig.getMaxAmount()) {
                loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
                loanFileDTO.setGrantingMessage("Cher client, le montant demandé n'est pas autorisé. Merci de revoir le montant saisi");
                return loanFileDTO;
            }
        }

        if(loanFileDTO.getAmount() > maxLoanSegment){
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setGrantingMessage("Votre montant d'éligibilité ne vous permet pas de souscrire au crédit demandé. Veuillez revoir le montant saisi");
            return loanFileDTO;
        }

        //using maxS1 as offer minScore
        int customerScore = Optional.ofNullable(customerData.getFmaScore()).orElse(0);
        if ( customerScore >= offerConfig.getMaxS1()) {
            loanFileDTO.setGrantingStatus(GrantingStatus.ACCEPTED);
            loanFileDTO.setEligibilityScore(customerScore);
            loanFileDTO.setGrantingMessage("Votre LEBALMA a reussi. Les fonds seront credites sur votre compte Free Money sous peu");
        } else {
            loanFileDTO.setGrantingStatus(GrantingStatus.NOT_GRANTED);
            loanFileDTO.setEligibilityScore(customerScore);
            loanFileDTO.setGrantingMessage("Votre score d'éligibilité ne vous permet pas de souscrire au crédit demandé. Nous vous invitons a utiliser votre Kalpe Free Money pour toutes vos transactions afin d etre eligible");
        }
        return loanFileDTO;
    }

    @Override
    public LoanFileDTO checkEligibility(LoanFileDTO loanFileDTO) {
        checkEligibilityProcess(loanFileDTO);
        if (loanFileDTO.getGrantingStatus().equals(GrantingStatus.ACCEPTED)) {
            OfferDTO offerConfig = getOfferConfig(loanFileDTO.getOfferSlug());
            if (offerConfig == null) throw new IllegalStateException("Offer shouldnt be null");
            createTransfers(loanFileDTO, offerConfig);
        }
        return this.save(loanFileDTO);
    }


    @Override
    public LoanFileDTO checkEligibilityUssd(LoanFileDTO loanFileDTO) {
        return this.checkEligibilityProcess(loanFileDTO);
    }

    //updates loan file granting status and granting processing message
    @Override
    public LoanFileDTO refresh(Long loanId) {
        LoanFileDTO loanFileDTO = findOne(loanId).orElse(null);
        if (loanFileDTO == null) return null;
        List<UMETransferDTO> umeTransferDTOS = this.uMETransferService.findLoanTransfers(loanId).orElse(null);
        if (umeTransferDTOS == null) return null;

        AtomicBoolean paymentState = new AtomicBoolean(true);
        AtomicBoolean moneyGranted = new AtomicBoolean(false);
        AtomicReference<String> paymentStateMessage = new AtomicReference<>("");

        umeTransferDTOS.forEach(t -> {
            if (t.getPaymentState() == PaymentState.SUCCESS) {
                String s = paymentStateMessage.get();
                if (!s.equals("")) s += " - "; // format message
                paymentStateMessage.set(s + t.getType() + ":SUCCESS");
                paymentState.set(paymentState.get());
            } else {
                String s = paymentStateMessage.get();
                if (!s.equals("")) s += " - "; // format message
                paymentStateMessage.set(s + t.getType() + ":FAILED");
                paymentState.set(false);
            }
            if (t.getType() == TransferType.LOAN_TRANSFER && t.getPaymentState() == PaymentState.SUCCESS) {
                moneyGranted.set(true);
            }
        });

        if (paymentState.get() && moneyGranted.get()) loanFileDTO.setGrantingStatus(GrantingStatus.GRANTED);
        if (paymentState.get() && !moneyGranted.get()) loanFileDTO.setGrantingStatus(GrantingStatus.GRANTED_W_ERROR);
        if (!paymentState.get() && moneyGranted.get()) loanFileDTO.setGrantingStatus(GrantingStatus.GRANTED_W_ERROR);
        if (!paymentState.get() && !moneyGranted.get()) loanFileDTO.setGrantingStatus(GrantingStatus.FAILED);
        loanFileDTO.setGrantingProcessMessage(paymentStateMessage.get());

        return save(loanFileDTO);
    }

    @Override
    public List<LoanFileDTO> findByMsisdnOrOfferSlugOrGrantingStatus(Pageable pageable, String msisdn, String offer, GrantingStatus status, Instant debut, Instant fin) {
        List<LoanFile> loanFileDTOLists = new ArrayList<>();
        if (StringUtils.isBlank(msisdn) && StringUtils.isBlank(offer) && status == null && debut == null && fin == null) {
            return this.findAll(Pageable.unpaged()).getContent();
        }
        if (debut != null) {
            if(debut.equals(fin)){
                loanFileDTOLists.addAll(loanFileRepository.findByCreatedDateAfter(debut));
            }else{
                loanFileDTOLists.addAll(loanFileRepository.findByCreatedDateBetween(debut, fin));
            }
        }
        else{
            loanFileDTOLists.addAll(loanFileRepository.findByCreatedDateBefore(fin));
        }
//        if (StringUtils.isNotBlank(msisdn)) {
//            loanFileDTOLists.addAll(loanFileRepository.findByMsisdn(msisdn));
//        }
//        if (StringUtils.isNotBlank(offer)) {
//            loanFileDTOLists.addAll(loanFileRepository.findByOfferSlug(offer));
//        }
//        if (status != null) {
//            loanFileDTOLists.addAll(loanFileRepository.findByGrantingStatus(status));
//        }
        Stream<LoanFileDTO> s = loanFileDTOLists.stream()
            .distinct()
            .sorted((l1,l2) -> l2.getCreatedDate().compareTo(l1.getCreatedDate()))
            .map(loanFileMapper::toDto);
        if (StringUtils.isNotBlank(msisdn)) {
            s = s.filter(loanFileDTO -> loanFileDTO.getMsisdn().equals(msisdn));
        }
        if (StringUtils.isNotBlank(offer)) {
            s = s.filter(loanFileDTO -> loanFileDTO.getOfferSlug().equals(offer));
        }
        if (status != null) {
            s = s.filter(loanFileDTO -> loanFileDTO.getGrantingStatus().equals(status));
        }
        return s.collect(Collectors.toList());
    }

    //this automatocally creates repayment file for loans granted that has no repayment file
    @Scheduled(fixedDelay = 60000, initialDelay = 10000)
    public void refreshAuto() {
        //this.loanFileRepository.findAllByRepaymentIdIsNullAndGrantingStatusIsIn(new GrantingStatus[]{GrantingStatus.GRANTED, GrantingStatus.GRANTED_W_ERROR})
        this.loanFileRepository.findAllByRepaymentIdIsNullAndGrantingStatusIsIn(new GrantingStatus[]{GrantingStatus.GRANTED})
            .stream()
            .map(loanFileMapper::toDto)
            .forEach(loanFileDTO -> {
                try {
                    loanFileServiceAsync.createRepayment(loanFileDTO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
    }

    private void createTransfers(LoanFileDTO loanFileDTO, OfferDTO offerConfig) {
        final double fees = Math.ceil(loanFileDTO.getAmount() * (offerConfig.getTotalFeesRate() / 100.0));
        final double grantedAmt = Math.floor(loanFileDTO.getAmount() - fees);
        UMETransferDTO bankFeesTransfer = UMETransferDTO.builder()
            .sourceWallet(offerConfig.getLoanWallet().getMsisdn())
            .sourcePin(offerConfig.getLoanWallet().getPin())
            .targetWallet(offerConfig.getFeesWallet().getMsisdn())
            .amount((double) Math.round(fees * offerConfig.getFeesBankRate() / 100))
            .type(TransferType.FEES_TRANSFER_BANK)
            .loanFileId(loanFileDTO.getId())
            .build();
        UMETransferDTO freemoneyFeesTransfer = UMETransferDTO.builder()
            .sourceWallet(offerConfig.getLoanWallet().getMsisdn())
            .sourcePin(offerConfig.getLoanWallet().getPin())
            .targetWallet(fmCentralFees)
            .amount((double) Math.round(fees * offerConfig.getFeesFreeMoneyRate() / 100))
            .type(TransferType.FEES_TRANSFER_FREE)
            .loanFileId(loanFileDTO.getId())
            .build();
        UMETransferDTO customerTransfer = UMETransferDTO.builder()
            .sourceWallet(offerConfig.getLoanWallet().getMsisdn())
            .sourcePin(offerConfig.getLoanWallet().getPin())
            .targetWallet(loanFileDTO.getMsisdn())
            .amount(grantedAmt)
            .type(TransferType.LOAN_TRANSFER)
            .loanFileId(loanFileDTO.getId())
            .build();

        loanFileDTO.setGrantedAmount(grantedAmt);
        loanFileDTO.setFees(fees);
        this.save(loanFileDTO);
        this.uMETransferService.save(customerTransfer);
        this.uMETransferService.save(freemoneyFeesTransfer);
        this.uMETransferService.save(bankFeesTransfer);
    }

    private OfferDTO getOfferConfig(String slug) {
        ResponseEntity<OfferDTO> offerConfig = gatewayClient.getOfferConfig(slug, jwt);
        if (offerConfig.getStatusCode().is2xxSuccessful()) {
            return offerConfig.getBody();
        }
        return null;
    }

    private AccessListDTO getBlackOrWhiteList(String cin) {
        ResponseEntity<AccessListDTO> blackOrWhiteList = null;
        try {
            blackOrWhiteList = gatewayClient.getBlackOrWhiteList(cin, jwt);
        } catch (Exception e) {
            log.warn(e.getMessage());
            return null;
        }
        if (blackOrWhiteList.getStatusCode().is2xxSuccessful()) {
            return blackOrWhiteList.getBody();
        }
        return null;
    }
}
