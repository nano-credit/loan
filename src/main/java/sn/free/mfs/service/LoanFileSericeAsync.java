package sn.free.mfs.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import sn.free.mfs.client.*;
import sn.free.mfs.domain.LoanFile;
import sn.free.mfs.repository.LoanFileRepository;
import sn.free.mfs.service.dto.LoanFileDTO;
import sn.free.mfs.service.mapper.LoanFileMapper;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

/**
 * @project: microcredit
 * @author: psow on 01/07/2020
 */

@Service
@Slf4j
public class LoanFileSericeAsync {

    private final RepaymentConfigFeignClient repaymentClient;
    private final GatewayConfigFeignClient gwClient;
    private final LoanFileRepository loanFileRepository;
    private final LoanFileMapper mapper;

    @Value("${app.jwt:}")
    private String jwt;

    public LoanFileSericeAsync(RepaymentConfigFeignClient repaymentClient, GatewayConfigFeignClient gwClient, LoanFileRepository loanFileRepository, LoanFileMapper mapper) {
        this.repaymentClient = repaymentClient;
        this.gwClient = gwClient;
        this.loanFileRepository = loanFileRepository;
        this.mapper = mapper;
    }

    @Async
    public void doCreateRepayment(LoanFileDTO loanFileDTO) {
        this.createRepayment(loanFileDTO);
    }

    public void createRepayment(LoanFileDTO result) {
        ResponseEntity<OfferDTO> offerConfigResp = gwClient.getOfferConfig(result.getOfferSlug(), this.jwt);
        OfferDTO offerDTO = null;

        if (offerConfigResp.getStatusCode().is2xxSuccessful()) {
            offerDTO = offerConfigResp.getBody();
        }
        if (offerDTO == null) throw new IllegalArgumentException("Offer not retrieved successfully");

        Instant dueDate = LocalDateTime.now()
            .plusDays(Long.valueOf(offerDTO.getDefaultLoanDuration()))
            .toLocalDate()
            .atTime(LocalTime.MAX)
            .toInstant(ZoneOffset.UTC);
        final RepaymentFileDTO repaymentFileDTO = RepaymentFileDTO.builder()
            .loanFileID(result.getId())
            .msisdn(result.getMsisdn())
            .identificationNo(result.getIdNumber())
            .offerSlug(offerDTO.getTenantId())
            .initialCapital(result.getGrantedAmount())
            .initialDueDate(dueDate)
            .applicationFees(result.getFees())
            .capitalOutstanding(result.getAmount())
            .feesOutstanding(0.0)
            .repaymentState(RepaymentState.ENCOURS)
            .repaymentStatus(RepaymentStatus.NA)
            .dueDate(dueDate)
            .offerSlug(offerDTO.getTenantId())
            .defaultLoanDuration(offerDTO.getDefaultLoanDuration())
            .nbJourPr1(offerDTO.getNbJourPr1())
            .nbJourPr2(offerDTO.getNbJourPr2())
            .pr0Rate(offerDTO.getPr0Rate())
            .pr1Rate(offerDTO.getPr1Rate())
            .pr2Rate(offerDTO.getPr2Rate())
            .rappelPr1(offerDTO.getRappelPr1())
            .rappelPr2(offerDTO.getRappelPr2())
            .rappelPr0(offerDTO.getRappelPr0())
            .build();
        log.debug("creating repayment file {}", repaymentFileDTO);
        final ResponseEntity<RepaymentFileDTO> repaymentFileResp = repaymentClient.createRepaymentFile(repaymentFileDTO, this.jwt);
        LoanFile loanFile = mapper.toEntity(result);
        if (repaymentFileResp.getStatusCode().is2xxSuccessful() && repaymentFileResp.getBody() != null) {
            loanFile.setRepaymentId(repaymentFileResp.getBody().getId());
            loanFile.setRepaymentLog("SUCCEEDED");
        } else {
            loanFile.setRepaymentLog("FAILED : " + repaymentFileResp.getStatusCode() + " : " + repaymentFileResp.getStatusCode().getReasonPhrase());
        }
        loanFileRepository.save(loanFile);
        log.debug("creating repayment file of {}", result);
    }
}
