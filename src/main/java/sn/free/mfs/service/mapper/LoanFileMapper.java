package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.LoanFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LoanFile} and its DTO {@link LoanFileDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LoanFileMapper extends EntityMapper<LoanFileDTO, LoanFile> {


    LoanFile toEntity(LoanFileDTO loanFileDTO);

    default LoanFile fromId(Long id) {
        if (id == null) {
            return null;
        }
        LoanFile loanFile = new LoanFile();
        loanFile.setId(id);
        return loanFile;
    }
}
