package sn.free.mfs.service.mapper;


import sn.free.mfs.domain.*;
import sn.free.mfs.service.dto.UMETransferDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UMETransfer} and its DTO {@link UMETransferDTO}.
 */
@Mapper(componentModel = "spring", uses = {LoanFileMapper.class})
public interface UMETransferMapper extends EntityMapper<UMETransferDTO, UMETransfer> {

    @Mapping(source = "loanFile.id", target = "loanFileId")
    UMETransferDTO toDto(UMETransfer uMETransfer);

    @Mapping(source = "loanFileId", target = "loanFile")
    UMETransfer toEntity(UMETransferDTO uMETransferDTO);

    default UMETransfer fromId(Long id) {
        if (id == null) {
            return null;
        }
        UMETransfer uMETransfer = new UMETransfer();
        uMETransfer.setId(id);
        return uMETransfer;
    }
}
