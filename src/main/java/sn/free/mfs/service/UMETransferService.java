package sn.free.mfs.service;

import sn.free.mfs.service.dto.UMETransferDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link sn.free.mfs.domain.UMETransfer}.
 */
public interface UMETransferService {

    /**
     * Save a uMETransfer.
     *
     * @param uMETransferDTO the entity to save.
     * @return the persisted entity.
     */
    UMETransferDTO save(UMETransferDTO uMETransferDTO);

    /**
     * Get all the uMETransfers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UMETransferDTO> findAll(Pageable pageable);

    /**
     * Get the "id" uMETransfer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UMETransferDTO> findOne(Long id);

    /**
     * Delete the "id" uMETransfer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find all transfer related to a loan.
     * @param id of loanfile
     * @return list of transfer
     */
    Optional<List<UMETransferDTO>> findLoanTransfers(Long id);
}
