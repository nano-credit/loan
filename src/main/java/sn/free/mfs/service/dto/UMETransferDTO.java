package sn.free.mfs.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.PaymentState;
import sn.free.mfs.domain.enumeration.TransferType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.mfs.domain.UMETransfer} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UMETransferDTO implements Serializable {

    private Long id;

    @NotNull
    private String sourceWallet;

    @NotNull
    private String sourcePin;

    @NotNull
    private String targetWallet;

    @NotNull
    private Double amount;

    private TransferType type;

    private PaymentState paymentState;

    private String paymentTxId;

    private String paymentMessage;

    private Long loanFileId;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;
}
