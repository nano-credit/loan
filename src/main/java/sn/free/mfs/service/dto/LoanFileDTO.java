package sn.free.mfs.service.dto;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.IDType;
import sn.free.mfs.domain.enumeration.GrantingStatus;

/**
 * A DTO for the {@link sn.free.mfs.domain.LoanFile} entity.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoanFileDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private Double amount;

    @NotNull
    private String offerSlug;

    private Double balance;

    private IDType idType;

    private String idNumber;

    private Integer eligibilityScore;

    private GrantingStatus grantingStatus;

    private String grantingMessage;

    private String grantingProcessMessage;

    private Double fees;

    private Double grantedAmount;

    private Long repaymentId;

    private String repaymentLog;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;


}
