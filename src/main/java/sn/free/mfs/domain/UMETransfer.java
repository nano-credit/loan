package sn.free.mfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.TransferType;

import sn.free.mfs.domain.enumeration.PaymentState;

/**
 * A UMETransfer.
 */
@Entity
@Table(name = "ume_transfer")

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UMETransfer extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_generator")
    @SequenceGenerator(name = "sequence_generator")
    private Long id;

    @NotNull
    @Column(name = "source_wallet", nullable = false)
    private String sourceWallet;

    @NotNull
    @Column(name = "source_pin", nullable = false)
    private String sourcePin;

    @NotNull
    @Column(name = "target_wallet", nullable = false)
    private String targetWallet;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TransferType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_state")
    private PaymentState paymentState;

    @Column(name = "payment_tx_id")
    private String paymentTxId;

    @Column(name = "payment_message")
    private String paymentMessage;

    @ManyToOne
    @JsonIgnoreProperties("transfers")
    @JoinColumn(name = "loan_file_id")
    private LoanFile loanFile;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove


    /*public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSourceWallet() {
        return sourceWallet;
    }

    public void setSourceWallet(String sourceWallet) {
        this.sourceWallet = sourceWallet;
    }

    public String getSourcePin() {
        return sourcePin;
    }

    public void setSourcePin(String sourcePin) {
        this.sourcePin = sourcePin;
    }

    public String getTargetWallet() {
        return targetWallet;
    }

    public void setTargetWallet(String targetWallet) {
        this.targetWallet = targetWallet;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public TransferType getType() {
        return type;
    }

    public void setType(TransferType type) {
        this.type = type;
    }

    public PaymentState getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(PaymentState paymentState) {
        this.paymentState = paymentState;
    }

    public String getPaymentTxId() {
        return paymentTxId;
    }

    public void setPaymentTxId(String paymentTxId) {
        this.paymentTxId = paymentTxId;
    }

    public String getPaymentMessage() {
        return paymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        this.paymentMessage = paymentMessage;
    }

    public LoanFile getLoanFile() {
        return loanFile;
    }

    public void setLoanFile(LoanFile loanFile) {
        this.loanFile = loanFile;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UMETransfer)) {
            return false;
        }
        return id != null && id.equals(((UMETransfer) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
