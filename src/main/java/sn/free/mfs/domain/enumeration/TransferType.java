package sn.free.mfs.domain.enumeration;

/**
 * The TransferType enumeration.
 */
public enum TransferType {
    FEES_TRANSFER_BANK, FEES_TRANSFER_FREE, LOAN_TRANSFER
}
