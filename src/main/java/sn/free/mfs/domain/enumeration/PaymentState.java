package sn.free.mfs.domain.enumeration;

/**
 * The PaymentState enumeration.
 */
public enum PaymentState {
    PENDING, SUCCESS, FAILED, ROLLEDBACK, ROLLED_BACK_W_ERROR
}
