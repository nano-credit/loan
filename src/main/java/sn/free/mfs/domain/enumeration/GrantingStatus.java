package sn.free.mfs.domain.enumeration;

/**
 * The GrantingStatus enumeration.
 */
public enum GrantingStatus {
    SUBMITTED, ACCEPTED, GRANTED, GRANTED_W_ERROR, NOT_GRANTED, FAILED, ROLLEDBACK, ROLLED_BACK_W_ERROR
}
