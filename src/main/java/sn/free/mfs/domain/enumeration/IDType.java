package sn.free.mfs.domain.enumeration;

/**
 * The IDType enumeration.
 */
public enum IDType {
    CNI, PASSPORT
}
