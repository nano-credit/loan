package sn.free.mfs.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "customer_data")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "Msisdn", nullable = false, length = 50)
    private String msisdn;

    @Column(name = "AgeTelecomMonth_i", nullable = true)
    private Long ageTelecomMonthI;

    @Column(name = "AgeMobileMoneyMonth_i", nullable = true)
    private Long ageMobileMoneyMonthI;

    @Column(name = "AGE_CLIENT", nullable = true)
    private Long ageClient;

    @Column(name = "Sexe_v", nullable = true, length = 10)
    private String sexeV;

    @Column(name = "NB_FAF_i", nullable = true)
    private Long nbFafI;

    @Column(name = "NbDistinctMsisdnPerIdNumber_i", nullable = true)
    private Long nbDistinctMsisdnPerIdNumberI;

    @Column(name = "Profession_v", nullable = true, length = 100)
    private String professionV;

    @Column(name = "Region_v", nullable = true, length = 100)
    private String regionV;

    @Column(name = "Ville_v", nullable = true, length = 100)
    private String villeV;

    @Column(name = "Type_Zone_v", nullable = true, length = 50)
    private String typeZoneV;

    @Column(name = "AvgDiffDayForTotalRocover_AllMFS_i", nullable = true)
    private Long avgDiffDayForTotalRocoverAllMfsI;

    @Column(name = "DiffDayLastLoan_All6M_i", nullable = true)
    private Long diffDayLastLoanAll6MI;

    @Column(name = "NbTotal_Lending_All6M_i", nullable = true)
    private Long nbTotalLendingAll6MI;

    @Column(name = "HaveRecharge_All6M_ti", nullable = true, length = 10)
    private String haveRechargeAll6MTi;

    @Column(name = "Top1TypeRechargeNbTotal_All6M_v", nullable = true, length = 100)
    private String top1TypeRechargeNbTotalAll6MV;

    @Column(name = "DiffDayLastRecharge_All6M_i", nullable = true)
    private Long diffDayLastRechargeAll6MI;

    @Column(name = "VoiceOutAvgTotalRealDuration_All3M_i", nullable = true)
    private Long voiceOutAvgTotalRealDurationAll3MI;

    @Column(name = "VoiceOutPercentCallOnNet_All3M_f", nullable = true)
    private Long voiceOutPercentCallOnNetAll3MF;

    @Column(name = "VoiceOutNbDistinctCallpartner_All3M_i", nullable = true)
    private Long voiceOutNbDistinctCallpartnerAll3MI;

    @Column(name = "RechargeNbTotal_All6M_i", nullable = true)
    private Long rechargeNbTotalAll6MI;

    @Column(name = "RechargeTotalAmount_All6M_f", nullable = true)
    private Long rechargeTotalAmountAll6MF;

    @Column(name = "MVL_C_NbTotalbuy_All6M_i", nullable = true)
    private Long mvlCNbTotalbuyAll6MI;

    @Column(name = "MVL_C_AmountTotalbuy_All6M_f", nullable = true)
    private Long mvlCAmountTotalbuyAll6MF;

    @Column(name = "MVL_C_NbTotalcustomerCashOut_All6M_i", nullable = true)
    private Long mvlCNbTotalcustomerCashOutAll6MI;

    @Column(name = "MVL_C_AmountTotalcustomerCashOut_All6M_f", nullable = true)
    private Long mvlCAmountTotalcustomerCashOutAll6MF;

    @Column(name = "MVL_D_NbTotalTransfer_All6M_i", nullable = true)
    private Long mvlDNbTotalTransferAll6MI;

    @Column(name = "MVL_D_AmountTotalTransfer_All6M_f", nullable = true)
    private Long mvlDAmountTotalTransferAll6MF;

    @Column(name = "MVL_D_NbTotalFee_All6M_i", nullable = true)
    private Long mvlDNbTotalFeeAll6MI;

    @Column(name = "MVL_D_AmountTotalFee_All6M_f", nullable = true)
    private Long mvlDAmountTotalFeeAll6MF;

    @Column(name = "MVL_D_NbTotalSell_All6M_i", nullable = true)
    private Long mvlDNbTotalSellAll6MI;

    @Column(name = "MVL_D_AmountTotalSell_All6M_f", nullable = true)
    private Long mvlDAmountTotalSellAll6MF;

    @Column(name = "MVL_C_NbTotalTransfer_All6M_i", nullable = true)
    private Long mvlCNbTotalTransferAll6MI;

    @Column(name = "MVL_C_AmountTotalTransfer_All6M_f", nullable = true)
    private Long mvlCAmountTotalTransferAll6MF;

    @Column(name = "MVL_C_NbTotalcustomerCashIn_All6M_i", nullable = true)
    private Long mvlCNbTotalcustomerCashInAll6MI;

    @Column(name = "MVL_C_AmountTotalcustomerCashIn_All6M_f", nullable = true)
    private Long mvlCAmountTotalcustomerCashInAll6MF;

    @Column(name = "Stdev_AgBal_StartBal_n", nullable = true)
    private Long stdevAgBalStartBalN;

    @Column(name = "AVG_AgBal_StartBal_n", nullable = true)
    private Long avgAgBalStartBalN;

    @Column(name = "AVG_AgBal_EndBal_n", nullable = true)
    private Long avgAgBalEndBalN;

    @Column(name = "AVG_AgBal_Credit_n", nullable = true)
    private Long avgAgBalCreditN;

    @Column(name = "AVG_AgBal_Debit_n", nullable = true)
    private Long avgAgBalDebitN;

    @Column(name = "SUM_AgBal_Credit_n", nullable = true)
    private Long sumAgBalCreditN;

    @Column(name = "SUM_AgBal_Debit_n", nullable = true)
    private Long sumAgBalDebitN;

    @Column(name = "AVG_AgBal_Difference_n", nullable = true)
    private Long avgAgBalDifferenceN;

    @Column(name = "SUM_AgBal_Difference_n", nullable = true)
    private Long sumAgBalDifferenceN;

    @Column(name = "Nb_AgBal_Credit_n", nullable = true)
    private Long nbAgBalCreditN;

    @Column(name = "Nb_AgBal_Debit_n", nullable = true)
    private Long nbAgBalDebitN;

    @Column(name = "Amount_IN_M1", nullable = true)
    private Long amountInM1;
    @Column(name = "Amount_IN_M2", nullable = true)
    private Long amountInM2;
    @Column(name = "Amount_IN_M3", nullable = true)
    private Long amountInM3;
    @Column(name = "Amount_IN_M4", nullable = true)
    private Long amountInM4;
    @Column(name = "Amount_IN_M5", nullable = true)
    private Long amountInM5;
    @Column(name = "Amount_IN_M6", nullable = true)
    private Long amountInM6;

    @Column(name = "nbCREDIT_M1", nullable = true)
    private Long nbCreditM1;
    @Column(name = "nbCREDIT_M2", nullable = true)
    private Long nbCreditM2;
    @Column(name = "nbCREDIT_M3", nullable = true)
    private Long nbCreditM3;
    @Column(name = "nbCREDIT_M4", nullable = true)
    private Long nbCreditM4;
    @Column(name = "nbCREDIT_M5", nullable = true)
    private Long nbCreditM5;
    @Column(name = "nbCREDIT_M6", nullable = true)
    private Long nbCreditM6;

    @Column(name = "HAVE_CREDIT_EACH_MONTH", nullable = true, length = 5)
    private String haveCreditEachMonth;
    @Column(name = "HAVE_CREDIT_EACH_MONTH_5", nullable = true, length = 5)
    private String haveCreditEachMonth5;
    @Column(name = "HAVE_CREDIT_EACH_MONTH_4", nullable = true, length = 5)
    private String haveCreditEachMonth4;
    @Column(name = "HAVE_CREDIT_EACH_MONTH_3", nullable = true, length = 5)
    private String haveCreditEachMonth3;
    @Column(name = "HAVE_CREDIT_EACH_MONTH_2", nullable = true, length = 5)
    private String haveCreditEachMonth2;
    @Column(name = "HAVE_CREDIT_EACH_MONTH_1", nullable = true, length = 5)
    private String haveCreditEachMonth1;

    @Column(name = "Nb_dist_TransType", nullable = true)
    private Long nbDistTransType;

    @Column(name = "Nb_dist_CellId", nullable = true)
    private Long nbDistCellId;

    @Column(name = "Frequence_Credit", nullable = true)
    private Long frequenceCredit;

    @Column(name = "Credit_moyen", nullable = true)
    private Long creditMoyen;

    @Column(name = "Frequence_Debit", nullable = true)
    private Long frequenceDebit;

    @Column(name = "Debit_moyen", nullable = true)
    private Long debitMoyen;

    @Column(name = "Top1TypeRechargeTotalAmount_All6M_v", nullable = true)
    private Long top1TypeRechargeTotalAmountAll6MV;

    @Column(name = "Language_v", nullable = true, length = 50)
    private String languageV;

    @Column(name = "H0", nullable = true)
    private Long h0;

    @Column(name = "H1", nullable = true)
    private Long h1;
    @Column(name = "H2", nullable = true)
    private Long h2;
    @Column(name = "H3", nullable = true)
    private Long h3;
    @Column(name = "H4", nullable = true)
    private Long h4;
    @Column(name = "H5", nullable = true)
    private Long h5;
    @Column(name = "H6", nullable = true)
    private Long h6;
    @Column(name = "H7", nullable = true)
    private Long h7;
    @Column(name = "H8", nullable = true)
    private Long h8;
    @Column(name = "H9", nullable = true)
    private Long h9;
    @Column(name = "H10", nullable = true)
    private Long h10;
    @Column(name = "H11", nullable = true)
    private Long h11;
    @Column(name = "H12", nullable = true)
    private Long h12;
    @Column(name = "H13", nullable = true)
    private Long h13;
    @Column(name = "H14", nullable = true)
    private Long h14;
    @Column(name = "H15", nullable = true)
    private Long h15;
    @Column(name = "H16", nullable = true)
    private Long h16;
    @Column(name = "H17", nullable = true)
    private Long h17;
    @Column(name = "H18", nullable = true)
    private Long h18;
    @Column(name = "H19", nullable = true)
    private Long h19;
    @Column(name = "H20", nullable = true)
    private Long h20;
    @Column(name = "H21", nullable = true)
    private Long h21;
    @Column(name = "H22", nullable = true)
    private Long h22;
    @Column(name = "H23", nullable = true)
    private Long h23;
    @Column(name = "H24", nullable = true)
    private Long h24;
    @Column(name = "FMA_Score", nullable = true)
    private Integer fmaScore = 0;
    @Column(name = "MAX_Loan_Segment", nullable = true)
    private Integer maxLoanSegment = 0;
    @Column(name = "CLUSTER", nullable = true)
    private Integer cluster = 0;
}
