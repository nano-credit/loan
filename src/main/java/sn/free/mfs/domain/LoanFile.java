package sn.free.mfs.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sn.free.mfs.domain.enumeration.IDType;

import sn.free.mfs.domain.enumeration.GrantingStatus;

/**
 * A LoanFile.
 */
@Entity
@Table(name = "loan_file")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoanFile extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_generator")
    @SequenceGenerator(name = "sequence_generator")
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @NotNull
    @Column(name = "offer_slug", nullable = false)
    private String offerSlug;

    @Column(name = "balance")
    private Double balance;

    @Enumerated(EnumType.STRING)
    @Column(name = "id_type")
    private IDType idType;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "eligibility_score")
    private Integer eligibilityScore;

    @Enumerated(EnumType.STRING)
    @Column(name = "granting_status")
    private GrantingStatus grantingStatus;

    @Column(name = "fees")
    private Double fees;

    @Column(name = "granted_amount")
    private Double grantedAmount;

    @Column(name = "granted_message")
    private String grantingMessage;

    @Column(name = "granted_process_message")
    private String grantingProcessMessage;

    @Column(name = "repayment_id")
    private Long repaymentId;

    @Column(name = "repayment_log")
    private String repaymentLog;

    @OneToMany(mappedBy = "loanFile")
    private Set<UMETransfer> transfers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove


    /*public static long getSerialVersionUID() {
        return serialVersionUID;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getOfferSlug() {
        return offerSlug;
    }

    public void setOfferSlug(String offerSlug) {
        this.offerSlug = offerSlug;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public IDType getIdType() {
        return idType;
    }

    public void setIdType(IDType idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Integer getEligibilityScore() {
        return eligibilityScore;
    }

    public void setEligibilityScore(Integer eligibilityScore) {
        this.eligibilityScore = eligibilityScore;
    }

    public GrantingStatus getGrantingStatus() {
        return grantingStatus;
    }

    public void setGrantingStatus(GrantingStatus grantingStatus) {
        this.grantingStatus = grantingStatus;
    }

    public Double getFees() {
        return fees;
    }

    public void setFees(Double fees) {
        this.fees = fees;
    }

    public Double getGrantedAmount() {
        return grantedAmount;
    }

    public void setGrantedAmount(Double grantedAmount) {
        this.grantedAmount = grantedAmount;
    }

    public String getGrantingMessage() {
        return grantingMessage;
    }

    public void setGrantingMessage(String grantingMessage) {
        this.grantingMessage = grantingMessage;
    }

    public String getGrantingProcessMessage() {
        return grantingProcessMessage;
    }

    public void setGrantingProcessMessage(String grantingProcessMessage) {
        this.grantingProcessMessage = grantingProcessMessage;
    }

    public Long getRepaymentId() {
        return repaymentId;
    }

    public void setRepaymentId(Long repaymentId) {
        this.repaymentId = repaymentId;
    }

    public String getRepaymentLog() {
        return repaymentLog;
    }

    public void setRepaymentLog(String repaymentLog) {
        this.repaymentLog = repaymentLog;
    }

    public Set<UMETransfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(Set<UMETransfer> transfers) {
        this.transfers = transfers;
    }
    */

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LoanFile)) {
            return false;
        }
        return id != null && id.equals(((LoanFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
