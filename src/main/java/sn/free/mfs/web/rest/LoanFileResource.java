package sn.free.mfs.web.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import sn.free.mfs.client.GatewayConfigFeignClient;
import sn.free.mfs.domain.enumeration.GrantingStatus;
import sn.free.mfs.service.LoanFileSericeAsync;
import sn.free.mfs.service.LoanFileService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.LoanFileDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.LoanFile}.
 */
@RestController
@RequestMapping("/api")
public class LoanFileResource {

    private final Logger log = LoggerFactory.getLogger(LoanFileResource.class);

    private static final String ENTITY_NAME = "loanLoanFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LoanFileService loanFileService;
    private final GatewayConfigFeignClient gatewayFeignClient;
    private final LoanFileSericeAsync loanFileServiceAsync;

    public LoanFileResource(LoanFileService loanFileService, GatewayConfigFeignClient gatewayFeignClient, LoanFileSericeAsync loanFileServiceAsync) {
        this.loanFileService = loanFileService;
        this.gatewayFeignClient = gatewayFeignClient;
        this.loanFileServiceAsync = loanFileServiceAsync;
    }

    /**
     * {@code POST  /loan-files} : Create a new loanFile.
     *
     * @param loanFileDTO the loanFileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new loanFileDTO, or with status {@code 400 (Bad Request)} if the loanFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loan-files")
    public ResponseEntity<LoanFileDTO> createLoanFile(@Valid @RequestBody LoanFileDTO loanFileDTO) throws URISyntaxException {
        log.debug("REST request to save LoanFile : {}", loanFileDTO);
        if (loanFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new loanFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LoanFileDTO result = loanFileService.save(loanFileDTO);
        result = loanFileService.checkEligibility(result);
        return ResponseEntity.created(new URI("/api/loan-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loan-files} : Updates an existing loanFile.
     *
     * @param loanFileDTO the loanFileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated loanFileDTO,
     * or with status {@code 400 (Bad Request)} if the loanFileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the loanFileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loan-files")
    public ResponseEntity<LoanFileDTO> updateLoanFile(@Valid @RequestBody LoanFileDTO loanFileDTO) throws URISyntaxException {
        log.debug("REST request to update LoanFile : {}", loanFileDTO);
        if (loanFileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoanFileDTO result = loanFileService.save(loanFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, loanFileDTO.getId().toString()))
            .body(result);
    }

    @PutMapping("/loan-files/merge")
    public ResponseEntity<LoanFileDTO> updateLoanFileMerge(@Valid @RequestBody LoanFileDTO loanFileDTO) throws URISyntaxException {
        log.debug("REST request to update LoanFile : {}", loanFileDTO);
        if (loanFileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LoanFileDTO result = loanFileService.saveMerge(loanFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, loanFileDTO.getId().toString()))
            .body(result);
    }

    @PutMapping("/loan-files/{loanId}/refresh")
    public ResponseEntity<LoanFileDTO> refreshLoanFile(@Valid @PathVariable Long loanId) {
        log.debug("REST request to refresh LoanFile with id : {}", loanId);

        LoanFileDTO result = loanFileService.refresh(loanId);
        //if (result.getGrantingStatus() == GrantingStatus.GRANTED || result.getGrantingStatus() == GrantingStatus.GRANTED_W_ERROR){
        if (result.getGrantingStatus() == GrantingStatus.GRANTED){
            loanFileServiceAsync.doCreateRepayment(result);
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, loanId.toString()))
            .body(result);
    }

    /**
     * {@code GET  /loan-files} : get all the loanFiles.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loanFiles in body.
     */
    @GetMapping("/loan-files")
    public ResponseEntity<List<LoanFileDTO>> getAllLoanFiles(Pageable pageable) {
        log.debug("REST request to get a page of LoanFiles");
        Page<LoanFileDTO> page = loanFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/loan-files/search")
    public ResponseEntity<List<LoanFileDTO>> getLoanFileByMsisdnOrOfferSlugOrGrantingStatus(Pageable pageable, @RequestParam (required = false) String msisdn, @RequestParam (required = false) String offer, @RequestParam (required = false) String status,
                                                                                            @RequestParam (required = false) String debut, @RequestParam (required = false) String fin) {
        GrantingStatus statusEnum = null;
        try {
            statusEnum = GrantingStatus.valueOf(status);
        }catch (IllegalArgumentException | NullPointerException iae) {
            log.debug(iae.getMessage());
        }
        log.debug("DEBUT :" + debut);
        log.debug("FIN :" + fin);
        log.debug("REST request to get LoanFile List : {}", msisdn + " or " + offer+" or "+status);
        //+ "T00:00:00.00Z"
        Instant debutt = !StringUtils.isBlank(debut) ? Instant.parse(debut) : null;
        Instant finn = !StringUtils.isBlank(fin) ? Instant.parse(fin) : Instant.now();
        List<LoanFileDTO> all = loanFileService.findByMsisdnOrOfferSlugOrGrantingStatus(pageable, msisdn, offer, statusEnum, debutt, finn);
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), all.size());
        final Page<LoanFileDTO> page = new PageImpl<>(all.subList(start, end), pageable, all.size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loan-files/:id} : get the "id" loanFile.
     *
     * @param id the id of the loanFileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the loanFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loan-files/{id}")
    public ResponseEntity<LoanFileDTO> getLoanFile(@PathVariable Long id) {
        log.debug("REST request to get LoanFile : {}", id);
        Optional<LoanFileDTO> loanFileDTO = loanFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(loanFileDTO);
    }

    /**
     * {@code DELETE  /loan-files/:id} : delete the "id" loanFile.
     *
     * @param id the id of the loanFileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loan-files/{id}")
    public ResponseEntity<Void> deleteLoanFile(@PathVariable Long id) {
        log.debug("REST request to delete LoanFile : {}", id);
        loanFileService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }


    /**
     * {@code GET  /loan-files} : get all the loanFiles.
     *
     * @param loanFileDTO the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of loanFiles in body.
     */
    @GetMapping("/check-eligibility")
    public LoanFileDTO checkEligibility(LoanFileDTO loanFileDTO) {
        log.debug("REST request to check eligibility of " + loanFileDTO.toString());
        LoanFileDTO result = loanFileService.checkEligibilityUssd(loanFileDTO);
        return result;

    }

}
