package sn.free.mfs.web.rest;

import sn.free.mfs.service.UMETransferService;
import sn.free.mfs.web.rest.errors.BadRequestAlertException;
import sn.free.mfs.service.dto.UMETransferDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.free.mfs.domain.UMETransfer}.
 */
@RestController
@RequestMapping("/api")
public class UMETransferResource {

    private final Logger log = LoggerFactory.getLogger(UMETransferResource.class);

    private static final String ENTITY_NAME = "loanUmeTransfer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UMETransferService uMETransferService;

    public UMETransferResource(UMETransferService uMETransferService) {
        this.uMETransferService = uMETransferService;
    }

    /**
     * {@code POST  /ume-transfers} : Create a new uMETransfer.
     *
     * @param uMETransferDTO the uMETransferDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new uMETransferDTO, or with status {@code 400 (Bad Request)} if the uMETransfer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ume-transfers")
    public ResponseEntity<UMETransferDTO> createUMETransfer(@Valid @RequestBody UMETransferDTO uMETransferDTO) throws URISyntaxException {
        log.debug("REST request to save UMETransfer : {}", uMETransferDTO);
        if (uMETransferDTO.getId() != null) {
            throw new BadRequestAlertException("A new uMETransfer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UMETransferDTO result = uMETransferService.save(uMETransferDTO);
        return ResponseEntity.created(new URI("/api/ume-transfers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ume-transfers} : Updates an existing uMETransfer.
     *
     * @param uMETransferDTO the uMETransferDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated uMETransferDTO,
     * or with status {@code 400 (Bad Request)} if the uMETransferDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the uMETransferDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ume-transfers")
    public ResponseEntity<UMETransferDTO> updateUMETransfer(@RequestBody UMETransferDTO uMETransferDTO) throws URISyntaxException {
        log.debug("REST request to update UMETransfer : {}", uMETransferDTO);
        if (uMETransferDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UMETransferDTO result = uMETransferService.save(uMETransferDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, uMETransferDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ume-transfers} : get all the uMETransfers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uMETransfers in body.
     */
    @GetMapping("/ume-transfers")
    public ResponseEntity<List<UMETransferDTO>> getAllUMETransfers(Pageable pageable) {
        log.debug("REST request to get a page of UMETransfers");
        Page<UMETransferDTO> page = uMETransferService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ume-transfers/:id} : get the "id" uMETransfer.
     *
     * @param id the id of the uMETransferDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uMETransferDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ume-transfers/{id}")
    public ResponseEntity<UMETransferDTO> getUMETransfer(@PathVariable Long id) {
        log.debug("REST request to get UMETransfer : {}", id);
        Optional<UMETransferDTO> uMETransferDTO = uMETransferService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uMETransferDTO);
    }

    @GetMapping("/ume-transfers/loanfile/{id}")
    public ResponseEntity<List<UMETransferDTO>> getLoanTransfers(@PathVariable Long id) {
        log.debug("REST request to get UMETransfer of loanfile : {}", id);
        Optional<List<UMETransferDTO>> uMETransferDTO = uMETransferService.findLoanTransfers(id);
        return ResponseUtil.wrapOrNotFound(uMETransferDTO);
    }

    /**
     * {@code DELETE  /ume-transfers/:id} : delete the "id" uMETransfer.
     *
     * @param id the id of the uMETransferDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ume-transfers/{id}")
    public ResponseEntity<Void> deleteUMETransfer(@PathVariable Long id) {
        log.debug("REST request to delete UMETransfer : {}", id);
        uMETransferService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
