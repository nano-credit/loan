package sn.free.mfs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sn.free.mfs.domain.CustomerData;

import java.util.Optional;

/**
 * @project: microcredit
 * @author: psow on 22/06/2020
 */
@Repository
public interface CustomerDataRepository extends CrudRepository<CustomerData, String> {

    Optional<CustomerData> findByMsisdn(String msisdn);
}
