package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.free.mfs.domain.UMETransfer;

import java.util.List;

import java.util.List;

/**
 * Spring Data  repository for the UMETransfer entity.
 */
@Repository
public interface UMETransferRepository extends JpaRepository<UMETransfer, Long> {

    List<UMETransfer> findAllByLoanFileId(Long loanFileId);

    List<UMETransfer> findAllByLoanFileIdOrderByCreatedDateDesc(Long loanFileId);

    Page<UMETransfer> findAllByOrderByCreatedDateDesc(Pageable pageable);
}
