package sn.free.mfs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.mfs.domain.LoanFile;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.mfs.domain.enumeration.GrantingStatus;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data  repository for the LoanFile entity.
 */
@Repository
public interface LoanFileRepository extends JpaRepository<LoanFile, Long> {

    List<LoanFile> findAllByRepaymentIdIsNullAndGrantingStatusIsIn(GrantingStatus[] statuses);

    Page<LoanFile> findByMsisdnOrOfferSlugOrGrantingStatus(Pageable pageable, String msisdn, String offer, GrantingStatus status);

    Page<LoanFile> findAllByOrderByCreatedDateDesc(Pageable pageable);

    List<LoanFile> findByMsisdn(String msisdn);
    Page<LoanFile> findByMsisdn(Pageable pageable, String msisdn);
    List<LoanFile> findByOfferSlug(String offer);
    Page<LoanFile> findByOfferSlug(Pageable pageable, String offer);
    List<LoanFile> findByGrantingStatus(GrantingStatus status);
    Page<LoanFile> findByGrantingStatus(Pageable pageable, GrantingStatus status);
    List<LoanFile> findByCreatedDateBetween(Instant debut, Instant fin);
    Page<LoanFile> findByCreatedDateBetween(Pageable pageable, Instant debut, Instant fin);
    List<LoanFile> findByCreatedDateBefore(Instant fin);
    Page<LoanFile> findByCreatedDateBefore(Pageable pageable, Instant fin);
    List<LoanFile> findByCreatedDateAfter(Instant fin);

}
