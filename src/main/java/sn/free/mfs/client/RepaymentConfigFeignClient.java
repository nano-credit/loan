package sn.free.mfs.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @project: microcredit
 * @author: psow on 17/06/2020
 */
@FeignClient("repayment")
public interface RepaymentConfigFeignClient {

    @RequestMapping(value = "/api/repayment-files", method = RequestMethod.POST)
    ResponseEntity<RepaymentFileDTO> createRepaymentFile(@RequestBody RepaymentFileDTO repaymentFileDTO, @RequestHeader("Authorization") String jwt);

    @RequestMapping(value = "/api/repayment-files", method = RequestMethod.POST)
    ResponseEntity<RepaymentFileDTO> createRepaymentFile(@RequestBody RepaymentFileDTO repaymentFileDTO);
}
