package sn.free.mfs.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @project: microcredit
 * @author: psow on 17/06/2020
 */
@FeignClient("https://microcredit")
public interface GatewayConfigFeignClient {

    @RequestMapping(value = "/api/offers/slug/{slug}", method = RequestMethod.GET)
    ResponseEntity<OfferDTO> getOfferConfig(@PathVariable("slug") String slug, @RequestHeader("Authorization") String jwt);

    @RequestMapping(value = "/api/access-lists/bow/search", method = RequestMethod.GET)
    ResponseEntity<AccessListDTO> getBlackOrWhiteList(@RequestParam("cin") String cin, @RequestHeader("Authorization") String jwt);
}
