package sn.free.mfs.client;

/**
 * The RepaymentStatus enumeration.
 */
public enum RepaymentStatus {
    NA, PARTIALLY, IN_ADVANCE, TERM, ULTIMATELY, PR1, PR2, RECOVERED, OVERDUE
}
