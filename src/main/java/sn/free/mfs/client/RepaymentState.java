package sn.free.mfs.client;

/**
 * The RepaymentState enumeration.
 */
public enum RepaymentState {
    ENCOURS, PR1, PR2, WRITE_OFF, CLOSED
}
