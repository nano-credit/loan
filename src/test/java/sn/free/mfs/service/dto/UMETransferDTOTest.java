package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class UMETransferDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UMETransferDTO.class);
        UMETransferDTO uMETransferDTO1 = new UMETransferDTO();
        uMETransferDTO1.setId(1L);
        UMETransferDTO uMETransferDTO2 = new UMETransferDTO();
        assertThat(uMETransferDTO1).isNotEqualTo(uMETransferDTO2);
        uMETransferDTO2.setId(uMETransferDTO1.getId());
        assertThat(uMETransferDTO1).isEqualTo(uMETransferDTO2);
        uMETransferDTO2.setId(2L);
        assertThat(uMETransferDTO1).isNotEqualTo(uMETransferDTO2);
        uMETransferDTO1.setId(null);
        assertThat(uMETransferDTO1).isNotEqualTo(uMETransferDTO2);
    }
}
