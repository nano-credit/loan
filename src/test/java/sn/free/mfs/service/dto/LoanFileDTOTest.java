package sn.free.mfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class LoanFileDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoanFileDTO.class);
        LoanFileDTO loanFileDTO1 = new LoanFileDTO();
        loanFileDTO1.setId(1L);
        LoanFileDTO loanFileDTO2 = new LoanFileDTO();
        assertThat(loanFileDTO1).isNotEqualTo(loanFileDTO2);
        loanFileDTO2.setId(loanFileDTO1.getId());
        assertThat(loanFileDTO1).isEqualTo(loanFileDTO2);
        loanFileDTO2.setId(2L);
        assertThat(loanFileDTO1).isNotEqualTo(loanFileDTO2);
        loanFileDTO1.setId(null);
        assertThat(loanFileDTO1).isNotEqualTo(loanFileDTO2);
    }
}
