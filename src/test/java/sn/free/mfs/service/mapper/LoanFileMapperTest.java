package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LoanFileMapperTest {

    private LoanFileMapper loanFileMapper;

    @BeforeEach
    public void setUp() {
        loanFileMapper = new LoanFileMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(loanFileMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(loanFileMapper.fromId(null)).isNull();
    }
}
