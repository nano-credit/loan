package sn.free.mfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UMETransferMapperTest {

    private UMETransferMapper uMETransferMapper;

    @BeforeEach
    public void setUp() {
        uMETransferMapper = new UMETransferMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(uMETransferMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(uMETransferMapper.fromId(null)).isNull();
    }
}
