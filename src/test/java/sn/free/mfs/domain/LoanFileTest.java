package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class LoanFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LoanFile.class);
        LoanFile loanFile1 = new LoanFile();
        loanFile1.setId(1L);
        LoanFile loanFile2 = new LoanFile();
        loanFile2.setId(loanFile1.getId());
        assertThat(loanFile1).isEqualTo(loanFile2);
        loanFile2.setId(2L);
        assertThat(loanFile1).isNotEqualTo(loanFile2);
        loanFile1.setId(null);
        assertThat(loanFile1).isNotEqualTo(loanFile2);
    }
}
