package sn.free.mfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.free.mfs.web.rest.TestUtil;

public class UMETransferTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UMETransfer.class);
        UMETransfer uMETransfer1 = new UMETransfer();
        uMETransfer1.setId(1L);
        UMETransfer uMETransfer2 = new UMETransfer();
        uMETransfer2.setId(uMETransfer1.getId());
        assertThat(uMETransfer1).isEqualTo(uMETransfer2);
        uMETransfer2.setId(2L);
        assertThat(uMETransfer1).isNotEqualTo(uMETransfer2);
        uMETransfer1.setId(null);
        assertThat(uMETransfer1).isNotEqualTo(uMETransfer2);
    }
}
