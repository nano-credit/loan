//package sn.free.mfs.web.rest;
//
//import sn.free.mfs.LoanApp;
//import sn.free.mfs.domain.UMETransfer;
//import sn.free.mfs.repository.UMETransferRepository;
//import sn.free.mfs.service.UMETransferService;
//import sn.free.mfs.service.dto.UMETransferDTO;
//import sn.free.mfs.service.mapper.UMETransferMapper;
//import sn.free.mfs.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.free.mfs.domain.enumeration.TransferType;
//import sn.free.mfs.domain.enumeration.PaymentState;
///**
// * Integration tests for the {@link UMETransferResource} REST controller.
// */
//@SpringBootTest(classes = LoanApp.class)
//public class UMETransferResourceIT {
//
//    private static final String DEFAULT_SOURCE_WALLET = "AAAAAAAAAA";
//    private static final String UPDATED_SOURCE_WALLET = "BBBBBBBBBB";
//
//    private static final String DEFAULT_TARGET_WALLET = "AAAAAAAAAA";
//    private static final String UPDATED_TARGET_WALLET = "BBBBBBBBBB";
//
//    private static final Double DEFAULT_AMOUNT = 1D;
//    private static final Double UPDATED_AMOUNT = 2D;
//
//    private static final TransferType DEFAULT_TYPE = TransferType.FEES_TRANSFER;
//    private static final TransferType UPDATED_TYPE = TransferType.LOAN_TRANSFER;
//
//    private static final PaymentState DEFAULT_PAYMENT_STATE = PaymentState.PENDING;
//    private static final PaymentState UPDATED_PAYMENT_STATE = PaymentState.SUCCESS;
//
//    private static final String DEFAULT_PAYMENT_TX_ID = "AAAAAAAAAA";
//    private static final String UPDATED_PAYMENT_TX_ID = "BBBBBBBBBB";
//
//    private static final String DEFAULT_PAYMENT_MESSAGE = "AAAAAAAAAA";
//    private static final String UPDATED_PAYMENT_MESSAGE = "BBBBBBBBBB";
//
//    @Autowired
//    private UMETransferRepository uMETransferRepository;
//
//    @Autowired
//    private UMETransferMapper uMETransferMapper;
//
//    @Autowired
//    private UMETransferService uMETransferService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restUMETransferMockMvc;
//
//    private UMETransfer uMETransfer;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final UMETransferResource uMETransferResource = new UMETransferResource(uMETransferService);
//        this.restUMETransferMockMvc = MockMvcBuilders.standaloneSetup(uMETransferResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static UMETransfer createEntity(EntityManager em) {
//        UMETransfer uMETransfer = new UMETransfer()
//            .sourceWallet(DEFAULT_SOURCE_WALLET)
//            .targetWallet(DEFAULT_TARGET_WALLET)
//            .amount(DEFAULT_AMOUNT)
//            .type(DEFAULT_TYPE)
//            .paymentState(DEFAULT_PAYMENT_STATE)
//            .paymentTxId(DEFAULT_PAYMENT_TX_ID)
//            .paymentMessage(DEFAULT_PAYMENT_MESSAGE);
//        return uMETransfer;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static UMETransfer createUpdatedEntity(EntityManager em) {
//        UMETransfer uMETransfer = new UMETransfer()
//            .sourceWallet(UPDATED_SOURCE_WALLET)
//            .targetWallet(UPDATED_TARGET_WALLET)
//            .amount(UPDATED_AMOUNT)
//            .type(UPDATED_TYPE)
//            .paymentState(UPDATED_PAYMENT_STATE)
//            .paymentTxId(UPDATED_PAYMENT_TX_ID)
//            .paymentMessage(UPDATED_PAYMENT_MESSAGE);
//        return uMETransfer;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        uMETransfer = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createUMETransfer() throws Exception {
//        int databaseSizeBeforeCreate = uMETransferRepository.findAll().size();
//
//        // Create the UMETransfer
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(uMETransfer);
//        restUMETransferMockMvc.perform(post("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the UMETransfer in the database
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeCreate + 1);
//        UMETransfer testUMETransfer = uMETransferList.get(uMETransferList.size() - 1);
//        assertThat(testUMETransfer.getSourceWallet()).isEqualTo(DEFAULT_SOURCE_WALLET);
//        assertThat(testUMETransfer.getTargetWallet()).isEqualTo(DEFAULT_TARGET_WALLET);
//        assertThat(testUMETransfer.getAmount()).isEqualTo(DEFAULT_AMOUNT);
//        assertThat(testUMETransfer.getType()).isEqualTo(DEFAULT_TYPE);
//        assertThat(testUMETransfer.getPaymentState()).isEqualTo(DEFAULT_PAYMENT_STATE);
//        assertThat(testUMETransfer.getPaymentTxId()).isEqualTo(DEFAULT_PAYMENT_TX_ID);
//        assertThat(testUMETransfer.getPaymentMessage()).isEqualTo(DEFAULT_PAYMENT_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void createUMETransferWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = uMETransferRepository.findAll().size();
//
//        // Create the UMETransfer with an existing ID
//        uMETransfer.setId(1L);
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(uMETransfer);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restUMETransferMockMvc.perform(post("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the UMETransfer in the database
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkSourceWalletIsRequired() throws Exception {
//        int databaseSizeBeforeTest = uMETransferRepository.findAll().size();
//        // set the field null
//        uMETransfer.setSourceWallet(null);
//
//        // Create the UMETransfer, which fails.
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(uMETransfer);
//
//        restUMETransferMockMvc.perform(post("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkTargetWalletIsRequired() throws Exception {
//        int databaseSizeBeforeTest = uMETransferRepository.findAll().size();
//        // set the field null
//        uMETransfer.setTargetWallet(null);
//
//        // Create the UMETransfer, which fails.
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(uMETransfer);
//
//        restUMETransferMockMvc.perform(post("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkAmountIsRequired() throws Exception {
//        int databaseSizeBeforeTest = uMETransferRepository.findAll().size();
//        // set the field null
//        uMETransfer.setAmount(null);
//
//        // Create the UMETransfer, which fails.
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(uMETransfer);
//
//        restUMETransferMockMvc.perform(post("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllUMETransfers() throws Exception {
//        // Initialize the database
//        uMETransferRepository.saveAndFlush(uMETransfer);
//
//        // Get all the uMETransferList
//        restUMETransferMockMvc.perform(get("/api/ume-transfers?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(uMETransfer.getId().intValue())))
//            .andExpect(jsonPath("$.[*].sourceWallet").value(hasItem(DEFAULT_SOURCE_WALLET)))
//            .andExpect(jsonPath("$.[*].targetWallet").value(hasItem(DEFAULT_TARGET_WALLET)))
//            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
//            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].paymentState").value(hasItem(DEFAULT_PAYMENT_STATE.toString())))
//            .andExpect(jsonPath("$.[*].paymentTxId").value(hasItem(DEFAULT_PAYMENT_TX_ID)))
//            .andExpect(jsonPath("$.[*].paymentMessage").value(hasItem(DEFAULT_PAYMENT_MESSAGE)));
//    }
//
//    @Test
//    @Transactional
//    public void getUMETransfer() throws Exception {
//        // Initialize the database
//        uMETransferRepository.saveAndFlush(uMETransfer);
//
//        // Get the uMETransfer
//        restUMETransferMockMvc.perform(get("/api/ume-transfers/{id}", uMETransfer.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.id").value(uMETransfer.getId().intValue()))
//            .andExpect(jsonPath("$.sourceWallet").value(DEFAULT_SOURCE_WALLET))
//            .andExpect(jsonPath("$.targetWallet").value(DEFAULT_TARGET_WALLET))
//            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
//            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
//            .andExpect(jsonPath("$.paymentState").value(DEFAULT_PAYMENT_STATE.toString()))
//            .andExpect(jsonPath("$.paymentTxId").value(DEFAULT_PAYMENT_TX_ID))
//            .andExpect(jsonPath("$.paymentMessage").value(DEFAULT_PAYMENT_MESSAGE));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingUMETransfer() throws Exception {
//        // Get the uMETransfer
//        restUMETransferMockMvc.perform(get("/api/ume-transfers/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateUMETransfer() throws Exception {
//        // Initialize the database
//        uMETransferRepository.saveAndFlush(uMETransfer);
//
//        int databaseSizeBeforeUpdate = uMETransferRepository.findAll().size();
//
//        // Update the uMETransfer
//        UMETransfer updatedUMETransfer = uMETransferRepository.findById(uMETransfer.getId()).get();
//        // Disconnect from session so that the updates on updatedUMETransfer are not directly saved in db
//        em.detach(updatedUMETransfer);
//        updatedUMETransfer
//            .sourceWallet(UPDATED_SOURCE_WALLET)
//            .targetWallet(UPDATED_TARGET_WALLET)
//            .amount(UPDATED_AMOUNT)
//            .type(UPDATED_TYPE)
//            .paymentState(UPDATED_PAYMENT_STATE)
//            .paymentTxId(UPDATED_PAYMENT_TX_ID)
//            .paymentMessage(UPDATED_PAYMENT_MESSAGE);
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(updatedUMETransfer);
//
//        restUMETransferMockMvc.perform(put("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the UMETransfer in the database
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeUpdate);
//        UMETransfer testUMETransfer = uMETransferList.get(uMETransferList.size() - 1);
//        assertThat(testUMETransfer.getSourceWallet()).isEqualTo(UPDATED_SOURCE_WALLET);
//        assertThat(testUMETransfer.getTargetWallet()).isEqualTo(UPDATED_TARGET_WALLET);
//        assertThat(testUMETransfer.getAmount()).isEqualTo(UPDATED_AMOUNT);
//        assertThat(testUMETransfer.getType()).isEqualTo(UPDATED_TYPE);
//        assertThat(testUMETransfer.getPaymentState()).isEqualTo(UPDATED_PAYMENT_STATE);
//        assertThat(testUMETransfer.getPaymentTxId()).isEqualTo(UPDATED_PAYMENT_TX_ID);
//        assertThat(testUMETransfer.getPaymentMessage()).isEqualTo(UPDATED_PAYMENT_MESSAGE);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingUMETransfer() throws Exception {
//        int databaseSizeBeforeUpdate = uMETransferRepository.findAll().size();
//
//        // Create the UMETransfer
//        UMETransferDTO uMETransferDTO = uMETransferMapper.toDto(uMETransfer);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restUMETransferMockMvc.perform(put("/api/ume-transfers")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(uMETransferDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the UMETransfer in the database
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteUMETransfer() throws Exception {
//        // Initialize the database
//        uMETransferRepository.saveAndFlush(uMETransfer);
//
//        int databaseSizeBeforeDelete = uMETransferRepository.findAll().size();
//
//        // Delete the uMETransfer
//        restUMETransferMockMvc.perform(delete("/api/ume-transfers/{id}", uMETransfer.getId())
//            .accept(TestUtil.APPLICATION_JSON))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<UMETransfer> uMETransferList = uMETransferRepository.findAll();
//        assertThat(uMETransferList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//}
