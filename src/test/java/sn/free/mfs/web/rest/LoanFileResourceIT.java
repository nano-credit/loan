//package sn.free.mfs.web.rest;
//
//import sn.free.mfs.LoanApp;
//import sn.free.mfs.domain.LoanFile;
//import sn.free.mfs.repository.LoanFileRepository;
//import sn.free.mfs.service.LoanFileService;
//import sn.free.mfs.service.dto.LoanFileDTO;
//import sn.free.mfs.service.mapper.LoanFileMapper;
//import sn.free.mfs.web.rest.errors.ExceptionTranslator;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
//import org.springframework.http.MediaType;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.validation.Validator;
//
//import javax.persistence.EntityManager;
//import java.util.List;
//
//import static sn.free.mfs.web.rest.TestUtil.createFormattingConversionService;
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.hamcrest.Matchers.hasItem;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//
//import sn.free.mfs.domain.enumeration.IDType;
//import sn.free.mfs.domain.enumeration.GrantingStatus;
///**
// * Integration tests for the {@link LoanFileResource} REST controller.
// */
//@SpringBootTest(classes = LoanApp.class)
//public class LoanFileResourceIT {
//
//    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
//    private static final String UPDATED_MSISDN = "BBBBBBBBBB";
//
//    private static final Double DEFAULT_AMOUNT = 1D;
//    private static final Double UPDATED_AMOUNT = 2D;
//
//    private static final String DEFAULT_OFFER_SLUG = "AAAAAAAAAA";
//    private static final String UPDATED_OFFER_SLUG = "BBBBBBBBBB";
//
//    private static final Double DEFAULT_BALANCE = 1D;
//    private static final Double UPDATED_BALANCE = 2D;
//
//    private static final IDType DEFAULT_ID_TYPE = IDType.CNI;
//    private static final IDType UPDATED_ID_TYPE = IDType.PASSPORT;
//
//    private static final String DEFAULT_ID_NUMBER = "AAAAAAAAAA";
//    private static final String UPDATED_ID_NUMBER = "BBBBBBBBBB";
//
//    private static final Integer DEFAULT_ELIGIBILITY_SCORE = 1;
//    private static final Integer UPDATED_ELIGIBILITY_SCORE = 2;
//
//    private static final GrantingStatus DEFAULT_GRANTING_STATUS = GrantingStatus.GRANTED;
//    private static final GrantingStatus UPDATED_GRANTING_STATUS = GrantingStatus.NOT_GRANTED;
//
//    private static final Double DEFAULT_FEES = 1D;
//    private static final Double UPDATED_FEES = 2D;
//
//    private static final Double DEFAULT_GRANTED_AMOUNT = 1D;
//    private static final Double UPDATED_GRANTED_AMOUNT = 2D;
//
//    @Autowired
//    private LoanFileRepository loanFileRepository;
//
//    @Autowired
//    private LoanFileMapper loanFileMapper;
//
//    @Autowired
//    private LoanFileService loanFileService;
//
//    @Autowired
//    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
//
//    @Autowired
//    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
//
//    @Autowired
//    private ExceptionTranslator exceptionTranslator;
//
//    @Autowired
//    private EntityManager em;
//
//    @Autowired
//    private Validator validator;
//
//    private MockMvc restLoanFileMockMvc;
//
//    private LoanFile loanFile;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        final LoanFileResource loanFileResource = new LoanFileResource(loanFileService);
//        this.restLoanFileMockMvc = MockMvcBuilders.standaloneSetup(loanFileResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setConversionService(createFormattingConversionService())
//            .setMessageConverters(jacksonMessageConverter)
//            .setValidator(validator).build();
//    }
//
//    /**
//     * Create an entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static LoanFile createEntity(EntityManager em) {
//        LoanFile loanFile = new LoanFile()
//            .msisdn(DEFAULT_MSISDN)
//            .amount(DEFAULT_AMOUNT)
//            .offerSlug(DEFAULT_OFFER_SLUG)
//            .balance(DEFAULT_BALANCE)
//            .idType(DEFAULT_ID_TYPE)
//            .idNumber(DEFAULT_ID_NUMBER)
//            .eligibilityScore(DEFAULT_ELIGIBILITY_SCORE)
//            .grantingStatus(DEFAULT_GRANTING_STATUS)
//            .fees(DEFAULT_FEES)
//            .grantedAmount(DEFAULT_GRANTED_AMOUNT);
//        return loanFile;
//    }
//    /**
//     * Create an updated entity for this test.
//     *
//     * This is a static method, as tests for other entities might also need it,
//     * if they test an entity which requires the current entity.
//     */
//    public static LoanFile createUpdatedEntity(EntityManager em) {
//        LoanFile loanFile = new LoanFile()
//            .msisdn(UPDATED_MSISDN)
//            .amount(UPDATED_AMOUNT)
//            .offerSlug(UPDATED_OFFER_SLUG)
//            .balance(UPDATED_BALANCE)
//            .idType(UPDATED_ID_TYPE)
//            .idNumber(UPDATED_ID_NUMBER)
//            .eligibilityScore(UPDATED_ELIGIBILITY_SCORE)
//            .grantingStatus(UPDATED_GRANTING_STATUS)
//            .fees(UPDATED_FEES)
//            .grantedAmount(UPDATED_GRANTED_AMOUNT);
//        return loanFile;
//    }
//
//    @BeforeEach
//    public void initTest() {
//        loanFile = createEntity(em);
//    }
//
//    @Test
//    @Transactional
//    public void createLoanFile() throws Exception {
//        int databaseSizeBeforeCreate = loanFileRepository.findAll().size();
//
//        // Create the LoanFile
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(loanFile);
//        restLoanFileMockMvc.perform(post("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the LoanFile in the database
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeCreate + 1);
//        LoanFile testLoanFile = loanFileList.get(loanFileList.size() - 1);
//        assertThat(testLoanFile.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
//        assertThat(testLoanFile.getAmount()).isEqualTo(DEFAULT_AMOUNT);
//        assertThat(testLoanFile.getOfferSlug()).isEqualTo(DEFAULT_OFFER_SLUG);
//        assertThat(testLoanFile.getBalance()).isEqualTo(DEFAULT_BALANCE);
//        assertThat(testLoanFile.getIdType()).isEqualTo(DEFAULT_ID_TYPE);
//        assertThat(testLoanFile.getIdNumber()).isEqualTo(DEFAULT_ID_NUMBER);
//        assertThat(testLoanFile.getEligibilityScore()).isEqualTo(DEFAULT_ELIGIBILITY_SCORE);
//        assertThat(testLoanFile.getGrantingStatus()).isEqualTo(DEFAULT_GRANTING_STATUS);
//        assertThat(testLoanFile.getFees()).isEqualTo(DEFAULT_FEES);
//        assertThat(testLoanFile.getGrantedAmount()).isEqualTo(DEFAULT_GRANTED_AMOUNT);
//    }
//
//    @Test
//    @Transactional
//    public void createLoanFileWithExistingId() throws Exception {
//        int databaseSizeBeforeCreate = loanFileRepository.findAll().size();
//
//        // Create the LoanFile with an existing ID
//        loanFile.setId(1L);
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(loanFile);
//
//        // An entity with an existing ID cannot be created, so this API call must fail
//        restLoanFileMockMvc.perform(post("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the LoanFile in the database
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeCreate);
//    }
//
//
//    @Test
//    @Transactional
//    public void checkMsisdnIsRequired() throws Exception {
//        int databaseSizeBeforeTest = loanFileRepository.findAll().size();
//        // set the field null
//        loanFile.setMsisdn(null);
//
//        // Create the LoanFile, which fails.
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(loanFile);
//
//        restLoanFileMockMvc.perform(post("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkAmountIsRequired() throws Exception {
//        int databaseSizeBeforeTest = loanFileRepository.findAll().size();
//        // set the field null
//        loanFile.setAmount(null);
//
//        // Create the LoanFile, which fails.
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(loanFile);
//
//        restLoanFileMockMvc.perform(post("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void checkOfferSlugIsRequired() throws Exception {
//        int databaseSizeBeforeTest = loanFileRepository.findAll().size();
//        // set the field null
//        loanFile.setOfferSlug(null);
//
//        // Create the LoanFile, which fails.
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(loanFile);
//
//        restLoanFileMockMvc.perform(post("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeTest);
//    }
//
//    @Test
//    @Transactional
//    public void getAllLoanFiles() throws Exception {
//        // Initialize the database
//        loanFileRepository.saveAndFlush(loanFile);
//
//        // Get all the loanFileList
//        restLoanFileMockMvc.perform(get("/api/loan-files?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(loanFile.getId().intValue())))
//            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN)))
//            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
//            .andExpect(jsonPath("$.[*].offerSlug").value(hasItem(DEFAULT_OFFER_SLUG)))
//            .andExpect(jsonPath("$.[*].balance").value(hasItem(DEFAULT_BALANCE.doubleValue())))
//            .andExpect(jsonPath("$.[*].idType").value(hasItem(DEFAULT_ID_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].idNumber").value(hasItem(DEFAULT_ID_NUMBER)))
//            .andExpect(jsonPath("$.[*].eligibilityScore").value(hasItem(DEFAULT_ELIGIBILITY_SCORE)))
//            .andExpect(jsonPath("$.[*].grantingStatus").value(hasItem(DEFAULT_GRANTING_STATUS.toString())))
//            .andExpect(jsonPath("$.[*].fees").value(hasItem(DEFAULT_FEES.doubleValue())))
//            .andExpect(jsonPath("$.[*].grantedAmount").value(hasItem(DEFAULT_GRANTED_AMOUNT.doubleValue())));
//    }
//
//    @Test
//    @Transactional
//    public void getLoanFile() throws Exception {
//        // Initialize the database
//        loanFileRepository.saveAndFlush(loanFile);
//
//        // Get the loanFile
//        restLoanFileMockMvc.perform(get("/api/loan-files/{id}", loanFile.getId()))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.id").value(loanFile.getId().intValue()))
//            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN))
//            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
//            .andExpect(jsonPath("$.offerSlug").value(DEFAULT_OFFER_SLUG))
//            .andExpect(jsonPath("$.balance").value(DEFAULT_BALANCE.doubleValue()))
//            .andExpect(jsonPath("$.idType").value(DEFAULT_ID_TYPE.toString()))
//            .andExpect(jsonPath("$.idNumber").value(DEFAULT_ID_NUMBER))
//            .andExpect(jsonPath("$.eligibilityScore").value(DEFAULT_ELIGIBILITY_SCORE))
//            .andExpect(jsonPath("$.grantingStatus").value(DEFAULT_GRANTING_STATUS.toString()))
//            .andExpect(jsonPath("$.fees").value(DEFAULT_FEES.doubleValue()))
//            .andExpect(jsonPath("$.grantedAmount").value(DEFAULT_GRANTED_AMOUNT.doubleValue()));
//    }
//
//    @Test
//    @Transactional
//    public void getNonExistingLoanFile() throws Exception {
//        // Get the loanFile
//        restLoanFileMockMvc.perform(get("/api/loan-files/{id}", Long.MAX_VALUE))
//            .andExpect(status().isNotFound());
//    }
//
//    @Test
//    @Transactional
//    public void updateLoanFile() throws Exception {
//        // Initialize the database
//        loanFileRepository.saveAndFlush(loanFile);
//
//        int databaseSizeBeforeUpdate = loanFileRepository.findAll().size();
//
//        // Update the loanFile
//        LoanFile updatedLoanFile = loanFileRepository.findById(loanFile.getId()).get();
//        // Disconnect from session so that the updates on updatedLoanFile are not directly saved in db
//        em.detach(updatedLoanFile);
//        updatedLoanFile
//            .msisdn(UPDATED_MSISDN)
//            .amount(UPDATED_AMOUNT)
//            .offerSlug(UPDATED_OFFER_SLUG)
//            .balance(UPDATED_BALANCE)
//            .idType(UPDATED_ID_TYPE)
//            .idNumber(UPDATED_ID_NUMBER)
//            .eligibilityScore(UPDATED_ELIGIBILITY_SCORE)
//            .grantingStatus(UPDATED_GRANTING_STATUS)
//            .fees(UPDATED_FEES)
//            .grantedAmount(UPDATED_GRANTED_AMOUNT);
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(updatedLoanFile);
//
//        restLoanFileMockMvc.perform(put("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the LoanFile in the database
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeUpdate);
//        LoanFile testLoanFile = loanFileList.get(loanFileList.size() - 1);
//        assertThat(testLoanFile.getMsisdn()).isEqualTo(UPDATED_MSISDN);
//        assertThat(testLoanFile.getAmount()).isEqualTo(UPDATED_AMOUNT);
//        assertThat(testLoanFile.getOfferSlug()).isEqualTo(UPDATED_OFFER_SLUG);
//        assertThat(testLoanFile.getBalance()).isEqualTo(UPDATED_BALANCE);
//        assertThat(testLoanFile.getIdType()).isEqualTo(UPDATED_ID_TYPE);
//        assertThat(testLoanFile.getIdNumber()).isEqualTo(UPDATED_ID_NUMBER);
//        assertThat(testLoanFile.getEligibilityScore()).isEqualTo(UPDATED_ELIGIBILITY_SCORE);
//        assertThat(testLoanFile.getGrantingStatus()).isEqualTo(UPDATED_GRANTING_STATUS);
//        assertThat(testLoanFile.getFees()).isEqualTo(UPDATED_FEES);
//        assertThat(testLoanFile.getGrantedAmount()).isEqualTo(UPDATED_GRANTED_AMOUNT);
//    }
//
//    @Test
//    @Transactional
//    public void updateNonExistingLoanFile() throws Exception {
//        int databaseSizeBeforeUpdate = loanFileRepository.findAll().size();
//
//        // Create the LoanFile
//        LoanFileDTO loanFileDTO = loanFileMapper.toDto(loanFile);
//
//        // If the entity doesn't have an ID, it will throw BadRequestAlertException
//        restLoanFileMockMvc.perform(put("/api/loan-files")
//            .contentType(TestUtil.APPLICATION_JSON)
//            .content(TestUtil.convertObjectToJsonBytes(loanFileDTO)))
//            .andExpect(status().isBadRequest());
//
//        // Validate the LoanFile in the database
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeUpdate);
//    }
//
//    @Test
//    @Transactional
//    public void deleteLoanFile() throws Exception {
//        // Initialize the database
//        loanFileRepository.saveAndFlush(loanFile);
//
//        int databaseSizeBeforeDelete = loanFileRepository.findAll().size();
//
//        // Delete the loanFile
//        restLoanFileMockMvc.perform(delete("/api/loan-files/{id}", loanFile.getId())
//            .accept(TestUtil.APPLICATION_JSON))
//            .andExpect(status().isNoContent());
//
//        // Validate the database contains one less item
//        List<LoanFile> loanFileList = loanFileRepository.findAll();
//        assertThat(loanFileList).hasSize(databaseSizeBeforeDelete - 1);
//    }
//}
